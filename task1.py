def gcd(a, b):
    while b != 0:
        a, b = b, a % b
    return a


def extended_gcd(a, b):
    if a == 0:
        return (b, 0, 1)
    else:
        g, y, x = extended_gcd(b % a, a)
        print("Intermediate Extended GCD:", g, x, y)  # Выводим промежуточные значения
        return (g, x - (b // a) * y, y)


def mod_inverse(a, m):
    g, x, y = extended_gcd(a, m)
    if g != 1:
        raise Exception('Modular inverse does not exist')
    else:
        return x % m


def generate_keypair(p, q, e):
    n = p * q
    phi = (p - 1) * (q - 1)
    d = mod_inverse(e, phi)
    print("Intermediate d:", d)  # Выводим значение d
    return ((e, n), (d, n))


def encrypt(public_key, plaintext):
    e, n = public_key
    return pow(plaintext, e, n)


def decrypt(private_key, ciphertext):
    d, n = private_key
    return pow(ciphertext, d, n)


if __name__ == '__main__':
    p = int(input("Enter prime number p: "))
    q = int(input("Enter prime number q: "))
    e = int(input("Enter public key e: "))

    public_key, private_key = generate_keypair(p, q, e)

    plaintext = int(input("Enter plaintext (an integer): "))

    encrypted = encrypt(public_key, plaintext)
    print("Encrypted message:", encrypted)

    decrypted = decrypt(private_key, encrypted)
    print("Decrypted message:", decrypted)


