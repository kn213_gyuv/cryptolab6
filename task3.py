def power_mod(base, exp, mod):
    result = 1
    while exp > 0:
        if exp % 2 == 1:
            result = (result * base) % mod
        exp //= 2
        base = (base * base) % mod
    return result


def diffie_hellman(p, g, x, y):
    # Вираховуємо відкриті ключі
    x_A = power_mod(g, x, p)
    y_B = power_mod(g, y, p)

    # Обмін відкритими ключами
    # x_A і y_B можна передати між абонентами

    # Обраховуємо секретні ключі
    K_A = power_mod(y_B, x, p)
    K_B = power_mod(x_A, y, p)

    return K_A, K_B


# Введення параметрів p і g
p = int(input("Введіть значення параметра p: "))
g = int(input("Введіть значення параметра g: "))

# Введення секретних ключів x і y
x = int(input("Введіть секретний ключ x: "))
y = int(input("Введіть секретний ключ y: "))

# Обчислення секретних ключів
K_A, K_B = diffie_hellman(p, g, x, y)

print("Секретний ключ для абонента A:", K_A)
print("Секретний ключ для абонента B:", K_B)
