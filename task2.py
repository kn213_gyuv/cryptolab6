import random

def generate_keypair(p, g):
    x = random.randint(2, p - 1)  # приватний ключ
    y = pow(g, x, p)               # публічний ключ
    return (x, y)

def encrypt(p, g, y, plaintext, k):
    c1 = pow(g, k, p)
    s = pow(y, k, p)
    c2 = (s * plaintext) % p
    return (c1, c2)

def decrypt(p, x, c1, c2):
    s = pow(c1, x, p)
    plaintext = (c2 * pow(s, -1, p)) % p
    return plaintext

# Введення параметрів p та g
p = int(input("Введіть просте число p: "))
g = int(input("Введіть ціле число g, таке що 1 < g < p-1: "))

# Введення значення x
x = int(input("Введіть приватний ключ x, такий що 1 < x < p-1: "))

# Генерація публічного ключа
y = pow(g, x, p)
print("Публічний ключ y:", y)

# Введення значення k
k = int(input("Введіть випадкове ціле число k, таке що 1 < k < p-1: "))

# Введення повідомлення для шифрування
plaintext = int(input("Введіть числове значення повідомлення для шифрування: "))

# Шифрування
c1, c2 = encrypt(p, g, y, plaintext, k)
print("Зашифрований текст:", (c1, c2))

# Дешифрування
decrypted_text = decrypt(p, x, c1, c2)
print("Дешифрований текст:", decrypted_text)
